package pl.sda.michalrys.circle_cross;

import java.util.Arrays;

public class test02 {

    public static final int BOARD_LIMIT_ROWS_MIN = 1;
    public static final int BOARD_LIMIT_ROWS_MAX = 3;
    public static final int BOARD_LIMIT_COLUMNS_MIN = 1;
    public static final int BOARD_LIMIT_COLUMNS_MAX = 4;

    public static void main(String[] args) {

        char markX = 'X';
        char markO = 'O';
        int[] userBoardSize = {3, 4};

        char[][] board = new char[userBoardSize[0]][userBoardSize[1]];
        board[0][0] = markO;
        board[0][1] = markO;
        board[0][2] = markX;
        board[0][3] = markX;

        board[1][0] = markX;
        board[1][1] = markX;
        board[1][2] = markO;
        board[1][3] = markX;

        board[2][0] = markX;
        board[2][1] = markX;
        board[2][2] = markX;
        board[2][3] = markX;

        System.out.println(Arrays.toString(board[0]));
        System.out.println(Arrays.toString(board[1]));
        System.out.println(Arrays.toString(board[2]));

        // check if there is a winner
        System.out.println("Check for " + markX + " if is winner: " + checkWinner(board, markX, userBoardSize, 2));
    }

    public static boolean checkWinner(char[][] circleCross, char mark, int[] userBoardSize, int marksHowManyToWin) {
        // check if there is diagonal
        int boardLimitRowsMax = userBoardSize[0];
        int boardLimitColumnsMax = userBoardSize[1];
        boolean isGameBoardSquare = false;
        if (boardLimitColumnsMax == boardLimitRowsMax) {
            isGameBoardSquare = true;
        }

        int markCounter = 0;

        // search winner in rows
        for (int i = 0; i < boardLimitRowsMax; i++) {
            for (int j = 0; j < boardLimitColumnsMax; j++) {
                if (circleCross[i][j] != mark) {
                    markCounter = 0;
                } else {
                    markCounter++;
                }
                if(markCounter == marksHowManyToWin) {
                    return true;
                }
            }
        }

        // search winner in columns
        for (int j = 0; j < boardLimitColumnsMax; j++) {
            for (int i = 0; i < boardLimitRowsMax; i++) {
                if (circleCross[i][j] != mark) {
                    markCounter = 0;
                } else {
                    markCounter++;
                }
                if(markCounter == marksHowManyToWin) {
                    return true;
                }
            }
        }

        // search winner in diagonals
        // i = offset, j - cell
        for (int i = 0; i < (boardLimitRowsMax); i++) {
            //System.out.println("- i -> " + i);

            for (int j = 0; j < boardLimitRowsMax; j++) {
                if ((j + i) < boardLimitRowsMax) {
                    //System.out.println("field 1 -> != mark " + ((j + i)) + "," + j);
                    if (circleCross[((j + i))][j] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if(markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }

            for (int j = 0; j < boardLimitRowsMax + i; j++) {
                if ((j - i) >= 0 && (j + 1) < boardLimitColumnsMax) {
                    //System.out.println("field 1 -> != mark " + ((j - i)) + "," + (j + 1));
                    if (circleCross[((j - i))][(j + 1)] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if(markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }

            for (int j = 0; j < boardLimitRowsMax; j++) {
                if (((boardLimitRowsMax - j + i)) < boardLimitColumnsMax) {
                    //System.out.println("field 1 -> != mark " + j + "," + ((boardLimitRowsMax - j + i)));
                    if (circleCross[j][((boardLimitRowsMax - j + i))] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if(markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }
            for (int j = 0; j < boardLimitRowsMax; j++) {
                if((boardLimitRowsMax - j - i - 1) >= 0) {
                    //System.out.println("field 1 -> != mark " + j + "," + ((boardLimitRowsMax - j - i - 1)));
                    if (circleCross[j][((boardLimitRowsMax - j - i - 1))] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if(markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}