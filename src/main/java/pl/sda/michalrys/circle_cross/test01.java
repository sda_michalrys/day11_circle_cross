package pl.sda.michalrys.circle_cross;

import java.util.Arrays;

public class test01 {

    public static final int BOARD_LIMIT_ROWS_MIN = 1;
    public static final int BOARD_LIMIT_ROWS_MAX = 3;
    public static final int BOARD_LIMIT_COLUMNS_MIN = 1;
    public static final int BOARD_LIMIT_COLUMNS_MAX = 4;

    public static void main(String[] args) {

        String markX = "X";
        String markO = "O";

        String[][] board = new String[3][4];
        board[0][0] = markO;
        board[0][1] = markO;
        board[0][2] = markX;
        board[0][3] = markX;

        board[1][0] = markX;
        board[1][1] = markX;
        board[1][2] = markO;
        board[1][3] = markX;

        board[2][0] = markX;
        board[2][1] = markX;
        board[2][2] = markX;
        board[2][3] = markX;

        System.out.println(Arrays.toString(board[0]));
        System.out.println(Arrays.toString(board[1]));
        System.out.println(Arrays.toString(board[2]));

        // check if there is a winner
        System.out.println("Check for " + markX + " if is winner: " + checkWinner(board, markX));

        // temp
        System.out.println("\n");
        System.out.println(board.length + ", " + board[0].length);


    }

    public static boolean checkWinner(String[][] circleCross, String mark) {
        // check if there is diagonal
        boolean isDiagonal = false;
        if (BOARD_LIMIT_COLUMNS_MAX == BOARD_LIMIT_ROWS_MAX) {
            isDiagonal = true;
        }

        // amount of mark in rows, columns, diagonals
        int[] markCounterInRow = new int[BOARD_LIMIT_ROWS_MAX];
        int[] markCounterInColumn = new int[BOARD_LIMIT_COLUMNS_MAX];
        int[] markCounterInDiagonal = new int[2];

        for (int i = 0; i < circleCross.length; i++) {

            for (int j = 0; j < circleCross[0].length; j++) {
                if (circleCross[i][j].equals(mark)) {
                    markCounterInRow[i] += 1;
                    markCounterInColumn[j] += 1;
                }
                if (!isDiagonal) {
                    continue;
                } else {
                    if (i == j && circleCross[i][j].equals(mark)) {
                        // diagonal 1
                        markCounterInDiagonal[0] += 1;
                    }
                    if (j == (circleCross.length - 1 - i) && circleCross[i][j].equals(mark)) {
                        // diagonal 2
                        markCounterInDiagonal[1] += 1;
                    }
                }
            }
        }

        // check if mark won in row
        for (int i = 0; i < BOARD_LIMIT_ROWS_MAX; i++) {
            if (markCounterInRow[i] == BOARD_LIMIT_COLUMNS_MAX) {
                System.out.println("Mark " + mark + " won in row " + (i + 1) + ".");
                return true;
            }
        }
        // check if mark won in column
        for (int i = 0; i < BOARD_LIMIT_COLUMNS_MAX; i++) {
            if (markCounterInColumn[i] == BOARD_LIMIT_ROWS_MAX) {
                System.out.println("Mark " + mark + " won in column " + (i + 1) + ".");
                return true;
            }
        }
        // check if diagonal exists
        if (!isDiagonal) {
            return false;
        } else {
            // check if mark won in diagonal
            for (int i = 0; i <= 1; i++) {
                if (markCounterInDiagonal[i] == BOARD_LIMIT_ROWS_MAX) {
                    System.out.println("Mark " + mark + " won in diagonal " + (i + 1) + ".");
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkWinner2(String[][] circleCross, String mark) {
        // check if there is diagonal
        boolean isGameBoardSquare = false;
        if (BOARD_LIMIT_COLUMNS_MAX == BOARD_LIMIT_ROWS_MAX) {
            isGameBoardSquare = true;
        }

        // amount of mark in rows, columns, diagonals
        int[] markCounterInRow = new int[BOARD_LIMIT_ROWS_MAX];
        int[] markCounterInColumn = new int[BOARD_LIMIT_COLUMNS_MAX];
        int[] markCounterInDiagonal = new int[2];

        for (int i = 0; i < circleCross.length; i++) {

            for (int j = 0; j < circleCross[0].length; j++) {
                if (circleCross[i][j].equals(mark)) {
                    markCounterInRow[i] += 1;
                    markCounterInColumn[j] += 1;
                }
                if (!isGameBoardSquare) {
                    continue;
                } else {
                    if (i == j && circleCross[i][j].equals(mark)) {
                        // diagonal 1
                        markCounterInDiagonal[0] += 1;
                    }
                    if (j == (circleCross.length - 1 - i) && circleCross[i][j].equals(mark)) {
                        // diagonal 2
                        markCounterInDiagonal[1] += 1;
                    }
                }
            }
        }

        // check if mark won in row
        for (int i = 0; i < BOARD_LIMIT_ROWS_MAX; i++) {
            if (markCounterInRow[i] == BOARD_LIMIT_COLUMNS_MAX) {
                System.out.println("Mark " + mark + " won in row " + (i + 1) + ".");
                return true;
            }
        }
        // check if mark won in column
        for (int i = 0; i < BOARD_LIMIT_COLUMNS_MAX; i++) {
            if (markCounterInColumn[i] == BOARD_LIMIT_ROWS_MAX) {
                System.out.println("Mark " + mark + " won in column " + (i + 1) + ".");
                return true;
            }
        }
        // check if diagonal exists
        if (!isGameBoardSquare) {
            return false;
        } else {
            // check if mark won in diagonal
            for (int i = 0; i <= 1; i++) {
                if (markCounterInDiagonal[i] == BOARD_LIMIT_ROWS_MAX) {
                    System.out.println("Mark " + mark + " won in diagonal " + (i + 1) + ".");
                    return true;
                }
            }
        }
        return false;
    }
}
