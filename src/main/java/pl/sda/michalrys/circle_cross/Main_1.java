package pl.sda.michalrys.circle_cross;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main_1 {
    public static final char MARK_EMPTY = ' ';
    public static final char MARK_X = 'X';
    public static final char MARK_O = 'O';
    public static final int MARKS_TO_WIN_HOW_MANY_MIN_LIMIT = 3;
    public static final int GAME_BOARD_LIMIT_ROWS_MIN = 1;
    public static final int GAME_BOARD_LIMIT_ROWS_MAX_AT_LEAST = 3;
    public static final int GAME_BOARD_LIMIT_COLUMNS_MIN = 1;
    public static final int GAME_BOARD_LIMIT_COLUMNS_MAX_AT_LEAST = 3;
    public static final char[] MARK_CURRENT = {MARK_O, MARK_X}; // MARK_CURRENT[gameLoop % 2 ] -> x,o,x,o,x,o, ...

    public static final String MESSAGE_START = "========================\n=== Tic tac toe game ===\n========================\n";
    public static final String MESSAGE_CURRENT_STATUS = "Current status of the game board:";

    public static final String MESSAGE_ASK_FOR_BOARD_SIZE = "Give game board size (example size for 3 rows and 4 rows: 3x4)";
    public static final String MESSAGE_WRONG_INPUT_DATA_GAME_BOARD = "\n! ERROR ! Wrong input data, please put correct value, example: 3x3.\n";
    public static final String MESSAGE_WRONG_INPUT_DATA_GAME_BOARD_LESS_3X3 = "\n! ERROR ! Size must be at least 3x3\n";

    public static final String MESSAGE_ASK_FOR_MARK_HOW_MANY = "How many marks win the game?";
    public static final String MESSAGE_WRONG_INPUT_DATA_MARK_HOW_MANY = "\n! ERROR ! Wrong input data, please try again.";
    public static final String MESSAGE_WRONG_INPUT_DATA_MARK_HOW_MANY_LESS_MIN = "\n! ERROR ! Amount of marks to win must be more than " + MARKS_TO_WIN_HOW_MANY_MIN_LIMIT;
    public static final String MESSAGE_WRONG_INPUT_DATA_MARK_HOW_MANY_MORE_THAN_GAMEBOARD_LIMIT =
            "\n! ERROR ! Amount of marks to win must be equal or less than the smallest dimension of the game board.";

    public static final String MESSAGE_ASK_FOR_MARK = "Give location of mark ";
    public static final String MESSAGE_EXAMPLE_OF_LOCATION = " (example of mark location: 1,3)";
    public static final String MESSAGE_WRONG_INPUT_DATA = "\n! ERROR ! Wrong input data, please give correct value, example: 1,3.\n";
    public static final String MESSAGE_WRONG_INPUT_DATA_FIELD_NOT_EMPTY = "\n! ERROR ! Wrong input data, this field is not empty.\n";
    public static final String MESSAGE_WRONG_INPUT_DATA_LOCATION_OUT_OF_GAME_BOARD = "\n! ERROR ! Given location is out of game board. Please try again. \n";

    public static final String MESSAGE_END_GAME_SEPARATOR = "\n\n=======================";
    public static final String MESSAGE_END_GAME = "End of game.";
    public static final String MESSAGE_END_GAME_WINNER_IS = "Winner is: ";
    public static final String MESSAGE_END_GAME_WINNER_IS_NOBODY = "Results: pat.";
    public static final String MESSAGE_END_GAME_RUN_AGAIN = "Try again :-)";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // run program
        System.out.println(MESSAGE_START);

        // initial game setup
        int[] userGameBoardSize = getGameBoardSize(scanner, MESSAGE_ASK_FOR_BOARD_SIZE); // size: no: rows | columns
        int gameRoundLimit = userGameBoardSize[0] * userGameBoardSize[1]; // maximum amount of game rounds
        char[][] gameBoard = initializeGameBoard(MARK_EMPTY, userGameBoardSize[0], userGameBoardSize[1]);
        int marksHowManyToWin = getFromUserHowManyMarksWinTheGame(scanner, gameBoard); //  how many marks win the game

        // setup for main loop
        int gameRound = 0; // id of current round
        char markCurrent; // X or O

        while (true) {
            // current round setup
            gameRound++;
            markCurrent = setMarkCurrent(gameRound);

            // get mark location from user: print board -> ask for location -> validation (try again)
            int[] markLocationNew = getMarkLocation(scanner, gameBoard, gameRound, markCurrent);

            // put mark on board : location is row,column
            gameBoard = putMarkOnGameBoard(gameBoard, markCurrent, markLocationNew[0], markLocationNew[1]);

            // check if somebody wins
            if (checkWinner(gameBoard, markCurrent, marksHowManyToWin)) {
                printGameSummaryWon(markCurrent, gameBoard);
                scanner.close();
                break;
            }

            // end/restart game if board is full
            if (gameRound == gameRoundLimit) {
                printGameSummaryDraw(gameBoard);
                // setup for runing game again
                gameRound = 0;
                gameBoard = initializeGameBoard(MARK_EMPTY, userGameBoardSize[0], userGameBoardSize[1]);
            }
        }
    }

    public static int getFromUserHowManyMarksWinTheGame(Scanner scanner, char[][] gameBoard) {
        int marksHowManyToWin;
        int boardLimitRowsMax = gameBoard.length;
        int boardLimitColumnMax = gameBoard[0].length;

        while (true) {
            System.out.println(MESSAGE_ASK_FOR_MARK_HOW_MANY);
            try {
                marksHowManyToWin = scanner.nextInt();
                scanner.nextLine();
            } catch (InputMismatchException exc) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_MARK_HOW_MANY);
                scanner.nextLine();
                continue;
            }
            if (marksHowManyToWin < MARKS_TO_WIN_HOW_MANY_MIN_LIMIT) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_MARK_HOW_MANY_LESS_MIN);
                continue;
            } else if (marksHowManyToWin > boardLimitRowsMax || marksHowManyToWin > boardLimitColumnMax) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_MARK_HOW_MANY_MORE_THAN_GAMEBOARD_LIMIT + " Current " +
                        "game board size is: " + boardLimitRowsMax + "x" + boardLimitColumnMax);
                continue;
            }
            break;
        }
        return marksHowManyToWin;
    }

    public static int[] getGameBoardSize(Scanner scanner, String message) {
        int[] gameBoardSize = new int[2];
        String userInput;

        while (true) {
            System.out.println(message);
            userInput = scanner.nextLine();

            try {
                gameBoardSize[0] = Integer.parseInt(userInput.split("x")[0]);
                gameBoardSize[1] = Integer.parseInt(userInput.split("x")[1]);

            } catch (NumberFormatException exc) {
                // value is not integer
                System.out.println(MESSAGE_WRONG_INPUT_DATA_GAME_BOARD);
                continue;
            } catch (ArrayIndexOutOfBoundsException exc) {
                // wrong format of input data
                System.out.println(MESSAGE_WRONG_INPUT_DATA_GAME_BOARD);
                continue;
            }
            // wrong size
            if (gameBoardSize[0] < GAME_BOARD_LIMIT_ROWS_MAX_AT_LEAST ||
                    gameBoardSize[1] < GAME_BOARD_LIMIT_COLUMNS_MAX_AT_LEAST) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_GAME_BOARD_LESS_3X3);
                continue;
            }
            break;
        }
        return gameBoardSize;
    }

    public static char[][] initializeGameBoard(char emptySpace, int boardLimitRowsMax, int boardLimitColumnsMax) {
        //char[][] circleCross = new char[BOARD_LIMIT_ROWS_MAX][BOARD_LIMIT_COLUMNS_MAX];
        char[][] circleCross = new char[boardLimitRowsMax][boardLimitColumnsMax];

        for (int i = 0; i < circleCross.length; i++) {
            for (int j = 0; j < circleCross[0].length; j++) {
                circleCross[i][j] = emptySpace;
            }
        }

        return circleCross;
    }

    public static char setMarkCurrent(int gameRound) {
        // current mark: X or O, depends on id of current round
        return MARK_CURRENT[gameRound % 2];
    }

    public static void printGameBoard(char[][] gameBoard, String messageInfo) {
        // input data to draw horizontal separator
        // todo: update print - length of mark is > 1
        int markMaxLength = 1;
//        if (MARK_O.length() > markMaxLength) {
//            markMaxLength = MARK_O.length();
//        }

        // print out
        System.out.println();
        System.out.println(messageInfo);
        for (int i = 0; i < gameBoard.length; i++) {
            for (int j = 0; j < gameBoard[0].length; j++) {
                System.out.print(" " + gameBoard[i][j] + " ");
                if (j < gameBoard[0].length - 1) {
                    System.out.print("|");
                } else {
                    System.out.println();
                }
            }
            // print out horizontal separator
            if (i < gameBoard.length - 1) {
                // this is valid for 3x3 board only
                //System.out.println("---+---+---");

                // todo: this is valid for mark which length is 1, update for > 1
                // - margin left
                System.out.print("-");
                for (int k = 0; k < gameBoard[0].length; k++) {
                    for (int j = 1; j <= markMaxLength; j++) {
                        // - below mark symbol
                        System.out.print("-");
                    }
                    if (k != gameBoard[0].length - 1) {
                        // - column separator
                        System.out.print("-+-");
                    }
                }
                // - margin right
                System.out.println("-");
            }

        }
        System.out.println("");
    }

    public static int[] getMarkLocation(Scanner scanner, char[][] gameBoard, int gameRound, char markCurrent) {
        int boardLimitRowsMax = gameBoard.length;
        int boardLimitColumnMax = gameBoard[0].length;
        int[] markLocation = new int[2];
        String userInput;

        while (true) {
            printGameBoard(gameBoard, MESSAGE_CURRENT_STATUS);
            System.out.println("Round " + gameRound + ") " + MESSAGE_ASK_FOR_MARK + markCurrent + MESSAGE_EXAMPLE_OF_LOCATION + ":");

            userInput = scanner.nextLine();
            try {
                markLocation[0] = Integer.parseInt(userInput.split(",")[0]);
                markLocation[1] = Integer.parseInt(userInput.split(",")[1]);

            } catch (NumberFormatException exc) {
                // coordinate is not integer
                System.out.println(MESSAGE_WRONG_INPUT_DATA);
                continue;
            } catch (ArrayIndexOutOfBoundsException exc) {
                //
                System.out.println(MESSAGE_WRONG_INPUT_DATA);
                continue;
            }

            // validation
            if (markLocation[0] < GAME_BOARD_LIMIT_ROWS_MIN || markLocation[0] > boardLimitRowsMax) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_LOCATION_OUT_OF_GAME_BOARD +
                        "Game board is " + boardLimitRowsMax + "x" + boardLimitColumnMax);
                continue;
            }
            if (markLocation[1] < GAME_BOARD_LIMIT_COLUMNS_MIN || markLocation[1] > boardLimitColumnMax) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_LOCATION_OUT_OF_GAME_BOARD +
                        "Game board is " + boardLimitRowsMax + "x" + boardLimitColumnMax);
                continue;
            }
            // check if chosen field is empty - if not, try again
            if (!isEmptyFieldOnGameBoard(gameBoard, markLocation[0], markLocation[1])) {
                System.out.println(MESSAGE_WRONG_INPUT_DATA_FIELD_NOT_EMPTY);
                continue;
            }
            break;
        }
        return markLocation;
    }

    public static boolean isEmptyFieldOnGameBoard(char[][] gameBoard, int markRow, int markColumn) {
        // input markRow, markColumn: 1 - 3
        if (gameBoard[markRow - 1][markColumn - 1] == (MARK_EMPTY)) {
            return true;
        }
        return false;
    }

    public static char[][] putMarkOnGameBoard(char[][] gameBoard, char mark, int markRow, int markColumn) {
        // input markRow, markColumn: 1 - 3

        char[][] circleCrossUpdated = gameBoard;
        circleCrossUpdated[markRow - 1][markColumn - 1] = mark;
        return circleCrossUpdated;
    }

    public static boolean checkWinner(char[][] gameBoard, char mark, int marksHowManyToWin) {
        // check if there is diagonal
        int boardLimitRowsMax = gameBoard.length;
        int boardLimitColumnsMax = gameBoard[0].length;
        boolean isGameBoardSquare = false;
        if (boardLimitColumnsMax == boardLimitRowsMax) {
            isGameBoardSquare = true;
        }

        int markCounter = 0;

        // TODO something went wrong ! check it
        // search winner in rows
        for (int i = 0; i < boardLimitRowsMax; i++) {
            markCounter = 0;
            for (int j = 0; j < boardLimitColumnsMax; j++) {
                if (gameBoard[i][j] != mark) {
                    markCounter = 0;
                } else {
                    markCounter++;
                }
                if (markCounter == marksHowManyToWin) {
                    return true;
                }
            }
        }

        // search winner in columns
        for (int j = 0; j < boardLimitColumnsMax; j++) {
            markCounter = 0;
            for (int i = 0; i < boardLimitRowsMax; i++) {
                if (gameBoard[i][j] != mark) {
                    markCounter = 0;
                } else {
                    markCounter++;
                }
                if (markCounter == marksHowManyToWin) {
                    return true;
                }
            }
        }


        // search winner in diagonals
        // i = offset, j - cell
        for (int i = 0; i < (boardLimitRowsMax); i++) {
            markCounter = 0;
            //System.out.println("- i -> " + i);

            for (int j = 0; j < boardLimitRowsMax; j++) {
                if ((j + i) < boardLimitRowsMax) {
                    //System.out.println("field 1 -> != mark " + ((j + i)) + "," + j);
                    if (gameBoard[((j + i))][j] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if (markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }

            for (int j = 0; j < boardLimitRowsMax + i; j++) {
                markCounter = 0;
                if ((j - i) >= 0 && (j + 1) < boardLimitColumnsMax) {
                    //System.out.println("field 1 -> != mark " + ((j - i)) + "," + (j + 1));
                    if (gameBoard[((j - i))][(j + 1)] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if (markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }

            for (int j = 0; j < boardLimitRowsMax; j++) {
                markCounter = 0;
                if (((boardLimitRowsMax - j + i)) < boardLimitColumnsMax) {
                    //System.out.println("field 1 -> != mark " + j + "," + ((boardLimitRowsMax - j + i)));
                    if (gameBoard[j][((boardLimitRowsMax - j + i))] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if (markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }
            for (int j = 0; j < boardLimitRowsMax; j++) {
                markCounter = 0;
                if ((boardLimitRowsMax - j - i - 1) >= 0) {
                    //System.out.println("field 1 -> != mark " + j + "," + ((boardLimitRowsMax - j - i - 1)));
                    if (gameBoard[j][((boardLimitRowsMax - j - i - 1))] != mark) {
                        markCounter = 0;
                    } else {
                        markCounter++;
                    }
                    if (markCounter == marksHowManyToWin) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void printGameSummaryWon(char markCurrent, char[][] gameBoard) {
        System.out.println(MESSAGE_END_GAME_SEPARATOR);
        System.out.println(MESSAGE_END_GAME);
        System.out.println(MESSAGE_END_GAME_WINNER_IS + markCurrent);
        printGameBoard(gameBoard, MESSAGE_CURRENT_STATUS);
    }

    public static void printGameSummaryDraw(char[][] gameBoard) {
        System.out.println(MESSAGE_END_GAME_SEPARATOR);
        System.out.println(MESSAGE_END_GAME);
        System.out.println(MESSAGE_END_GAME_WINNER_IS_NOBODY);
        printGameBoard(gameBoard, MESSAGE_CURRENT_STATUS);
        // run game again
        System.out.println(MESSAGE_END_GAME_RUN_AGAIN);
        System.out.println(MESSAGE_END_GAME_SEPARATOR);
    }
}