package pl.sda.michalrys.circle_cross;

public class TempPrevious {

    public static boolean checkWinner(char[][] circleCross, char mark, int[] userBoardSize) {
        // check if there is diagonal
        int boardLimitRowsMax = userBoardSize[0];
        int boardLimitColumnsMax = userBoardSize[1];
        boolean isGameBoardSquare = false;
        if (boardLimitColumnsMax == boardLimitRowsMax) {
            isGameBoardSquare = true;
        }

        // amount of mark in rows, columns, diagonals
        int[] markCounterInRow = new int[boardLimitRowsMax];
        int[] markCounterInColumn = new int[boardLimitColumnsMax];
        int[] markCounterInDiagonal = new int[2];

        for (int i = 0; i < circleCross.length; i++) {

            for (int j = 0; j < circleCross[0].length; j++) {
                if (circleCross[i][j] == (mark)) {
                    markCounterInRow[i] += 1;
                    markCounterInColumn[j] += 1;
                }
                if (!isGameBoardSquare) {
                    continue;
                } else {
                    if (i == j && circleCross[i][j] == (mark)) {
                        // diagonal 1
                        markCounterInDiagonal[0] += 1;
                    }
                    if (j == (circleCross.length - 1 - i) && circleCross[i][j] == (mark)) {
                        // diagonal 2
                        markCounterInDiagonal[1] += 1;
                    }
                }
            }
        }

        // check if mark won in row
        for (int i = 0; i < boardLimitRowsMax; i++) {
            if (markCounterInRow[i] == boardLimitColumnsMax) {
                System.out.println("Mark " + mark + " won in row " + (i + 1) + ".");
                return true;
            }
        }
        // check if mark won in column
        for (int i = 0; i < boardLimitColumnsMax; i++) {
            if (markCounterInColumn[i] == boardLimitRowsMax) {
                System.out.println("Mark " + mark + " won in column " + (i + 1) + ".");
                return true;
            }
        }
        // check if diagonal exists
        if (!isGameBoardSquare) {
            return false;
        } else {
            // check if mark won in diagonal
            for (int i = 0; i <= 1; i++) {
                if (markCounterInDiagonal[i] == boardLimitRowsMax) {
                    System.out.println("Mark " + mark + " won in diagonal " + (i + 1) + ".");
                    return true;
                }
            }
        }
        return false;
    }

}
